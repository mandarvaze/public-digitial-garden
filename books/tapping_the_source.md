---
title : Tapping the Source
created: 2023-06-23
---

This book is re-interpretation of Charles Haanel's book "The Master Key System"

## Daily Manifestation Process (and Focus Phrases)

1. Remember to Remember
> "I choose to focus enjoyably inward"
2. Quite Your Mind
> "My mind is quite ... I'm now in the Silence"
3. Enter into Receive Mode
> "I am open to receive guidance from my Source"
4. Feel Your Core Passion
> "I know what I want"
5. Tap into Manifestation Power
>  "I feel connected with creative power"
6. Focus upon You Higher Intent
> "My vision is right now perfect and complete"
7. Manifest Your Goal
> "Each new moment is manifesting my dream"


## Resources

* ~4 min [Guided Meditation](https://www.youtube.com/watch?v=daQ-MFXip4k) on Youtube