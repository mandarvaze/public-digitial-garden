* <kbd>Cmd+Shift+s</kbd> : Create Scratch note
* <kbd>Cmd+Shift+j</kbd> : Create Journal note
* To create a new note from a wikilink like `[[my.new.note]]`, use `Dendron: Goto Note` menu.
  * `^` here indicates `Ctrl` not caret (I kept pressing <kbd>Shift+6</kbd> for `^` and <kbd>Enter</kbd>. Needless to say, it didn't work. 😄
  * If the note exists, you just *Go to* the existing note 😀 

# Manually insert/update the timestamps in frontmatter

If you do not use VSCode to update existing notes, the frontmatter timestamps do not get auto updated.
Unfortunately they are epoch time, so ~~difficult~~ impossible to update by hand.

I asked on the discord and came to know that using value returned by `date +"%s"000` can be safely used.

If using Emacs (like me) one can type the command in `vterm` and copy the results using `yy` and paste it in the markdown note where needed.

In vim/nvim one can use `!!date +"\%s"` to output that at the line the cursor is on, remembering to escape the % to stop it from expanding. (I have not tried this myself)
