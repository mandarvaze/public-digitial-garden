---
created: 2024-10-30
modified: 2024-10-30
---

### Create New Window

On macOS `cmd+n` automatically creates a new instance.
This is not configured (or does not work) on Linux.
We have two options :

1. `alacritty msg create-window || alacritty` from existing terminal session
2. Add a keybinding as follows:
```toml
[keyboard]
bindings = [
  { key = "N", mods = "Super", action = "CreateNewWindow" },
]
```
