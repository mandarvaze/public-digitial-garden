### Draw with both hands simultaneously

* Balanced doodling : Both hands draw the same thing
  * They mirror each other
  * Start with simple shapes at first and gradually move to complex doodling
* Unbalanced doodling : Hands draw different things
  * e.g. One hand draws a triangle then other one draws a circle or rectangle

[[sketching]]
[[sketchnoting]]
