## 10 minutes Morning Fitness Routine (Fat Burning)

No equipment needed : 

1. 45 sec high knees (15 sec rest)
2. 45 sec low to high plank (15 sec rest)
3. 45 sec half burpees (15 sec rest)
4. 45 sec toe touch (15 sec rest)
5. 45 sec squat jumps (15 sec rest)
6. 20-20 sec side plank each side (15 sec rest)
7. 45 sec alternate lunges (15 sec rest)
8. 45 sec jumping jacks (15 sec rest)
9. 45 sec hip thrust (15 sec rest)
10. 45 sec but kicks (15 sec rest)

Source : [Youtube Video](https://www.youtube.com/watch?v=7KSNmziMqog)
