---
modified: 2024-05-18
---

* gerund

`gerund` is a verb that acts like a noun. e.g. `I love sketching`
Here the verb (action word) in the sentence in `love` and `sketching` although a verb itself, isn't the action described in the sentence.
Instead, it acts as a noun.

  
