## Hyper Terminal

[`hyper.js`](https://hyper.is) has been the platform neutral Terminal app of choice for me since last year of so.
I used and loved iTerm2 a lot and used it for years, till I started considering moving to linux

I used Hyper exclusively on macOS for months before I decided to switch to linux (I'm most likely to go back to macOS)
and even on linux I'm not disappointed. It is better than built-in terminal

Anyway, one of the "problem" I faced recently was when using emacs in a terminal (`emacs -nw`) I couldn't use `M-x` (which is `Alt-x`) commands
cause` Alt` is mapped to hamburger Menu by default on linux.

So nice to see that keymaps can be customized. I added the following to my `~/.hyper.js`. Problem Solved!

```json
  keymaps: {
  // default `alt` clashes with emacs M-x
  // Plus I hardly need to invoke hamburgermenu via keyboard
  "window:hamburgerMenu": "f12"
  },
```

See [docs](https://hyper.is/#keymaps) for additional details

