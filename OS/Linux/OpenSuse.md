---
created: 2024-10-29
modified: 2024-11-02
---

* Change default browser:  

> If you use KDE you can change it via SystemSettings->Default-Applications->Web browser.

- Install Librewolf from Fedora (instead of Flatpak)
	- See https://librewolf.net/installation/opensuse/
	- Manually enable native messagging
	  `ln -s ~/.mozilla/native-messaging-hosts ~/.librewolf/native-messaging-hosts)`
	- You may need to create ~/.librewolf if it does not exist before the above command via `mkdir ~/.librewolf`

### Espanso for Wayland 

```
sudo zypper install libcap-progs
sudo setcap "cap_dac_override+p" $(which espanso)
espanso-wayland start --unmanaged
```

I also had to remove `._` files created when I copied files from macOS
`espanso log` was helpful

I needed to add the following to `config/default.yml`
```
keyboard_layout:
  layout: us
```
