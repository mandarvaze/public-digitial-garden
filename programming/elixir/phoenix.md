## Render Text

During early development, it is easy to just render a text.

Most tutorials use `render(conn, "show.html")`- which requires one to create
view function, as well as `show.html.eex`

While this may be required later, there is an easy way 

`text(conn, "Hello #{user}")`

There is also `json(conn, my_json)`

## Action name conventions

* `index` - renders a list of all items of the given resource type
* `show` - renders an individual item by id
* `new` - renders a form for creating a new item
* `create` - receives params for one new item and saves it in a datastore
* `edit` - retrieves an individual item by id and displays it in a form for editing
* `update` - receives params for one edited item and saves it to a datastore
* `delete` - receives an id for an item to be deleted and deletes it from a datastore


## Resources

* [Learn Phoenix Framework](https://github.com/dwyl/learn-phoenix-framework) - Has a link to learn elixir repo by the same developer too
