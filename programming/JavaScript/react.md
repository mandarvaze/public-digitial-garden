## Enable Emmet Support for React in VSCode

Emmet support is built-in into VSCode. But it does not work out of the box with react.
So you need to configure it such that `javascriptreact` is treated as `javascript` (At least that is that I think it indicates)

1. Go to Settings Via <kbd>Cmd+,</kbd> on Mac (or <kbd>Ctrl+,</kbd> on Windows)
2. Search for `Emmet` under `Extensions` (and then Search for `Include` on that page)

There are instructions/examples there.  Use `javascript` as Item and `javascriptreact` as Value

Or Add the following to the `settings.json` (Open via `Edit in settings.json` link on that page)
```json
   "emmet.includeLanguages": {
        "javascript": "javascriptreact"
    },
```

[Source](https://medium.com/@eshwaren/enable-emmet-support-for-jsx-in-visual-studio-code-react-f1f5dfe8809c)

----
