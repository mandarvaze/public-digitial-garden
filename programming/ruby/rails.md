# Rails 7 Cheat sheet

```sh
$ rails new appname
$ rails g scaffold model_name field1:fldtype .....
$ rails db:migrate
$ rails server
```

Now http://localhost:3000/model_name has a working CRUD app

Add the following to `app/views/layouts/application.html.erb` before `<%= javascript_importmap_tags %>` in the `<head>` section.

```html
<link rel="stylesheet" href="https://cdn.simplecss.org/simple.min.css">
```

Use `rails console` to create DB entries from the terminal like `Model.create! field1: "Value"`

Update `config/routes.rb` and add `root` entry (e.g. `root "articles#index"`) so that `http://localhost:3000/` does not end up showing Rails banner.

# Tutorials

[This](https://codewithrails.com/rails-authentication) is quite extensive tutorial for `Rails app with Authentication`. Has OAuth support out of the box
