---
created: 2024-09-08
modified: 2024-09-11
---

## Install on macOS

* (Optional) `brew install --cask temurin@21` - install Temurin 21 (formerly AdoptOpenJDK)
* `brew install clojure/tools/clojure`
* `brew install leiningen`
* (Optional) `brew install borkdude/brew/clj-kondo` : Clojure linter
* (Optional) `brew install clojure-lsp/brew/clojure-lsp-native` : Clojure LSP

## Performance related tips

* Prefer `get` over `nth` [^1]
> using nth to retrieve an element from a list is slower than using get to
> retrieve an element from a vector. This is because Clojure has to traverse
> all n elements of a list to get to the nth, whereas it only takes a few hops
> at most to access a vector element by its index.


[^1]: From [Clojure for the Brave and True](https://www.braveclojure.com/do-things/) 
